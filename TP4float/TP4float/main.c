#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include "adc.h"


void separEnteroDecimal(float dato,uint8_t *entero, uint8_t *decimal);
void stringGrado(uint8_t entero, uint8_t decimal, uint8_t*str);
void temInit();
void initTimer();

uint8_t gradoEntero,gradoDecimal;
uint8_t gradosString[4] = {'0','0','.','0'};
float grados;
volatile static uint8_t flagTimer = 0, counterTimer = 0;
int main(void)
{
	DDRB |= (1<<PINB0) | (1<<PINB1);
    LCDinit();
	sei();
	adc_init();
	adc_start();
	temInit();
	initTimer();
	
    while (1) 
    {	
		if (flagTimer){
			separEnteroDecimal(((send_result()*5000UL)/1024),&gradoEntero,&gradoDecimal);
			LCDGotoXY(6,0);
			stringGrado(gradoEntero,gradoDecimal,gradosString);
			LCDstring(gradosString,4);
			flagTimer = 0;
		}
		
		if (send_cambioValor()){
			grados = ((send_result()*500UL)/1024);
			if (grados > 24) {
				PORTB = (1<<PINB0);
			}else if (grados < 17){
						PORTB = (1<<PINB1);
					}
				else PORTB = 0;
		}
    }
}

void separEnteroDecimal(float dato,uint8_t *entero, uint8_t *decimal){
	//calcular el mod 10 de dato va a ser el decimal
	*decimal = (int) dato % 10;
	*entero = (int) dato / 10;
}

void stringGrado(uint8_t entero, uint8_t decimal, uint8_t*str){
	if(entero>9){
		str[0] = (entero / 10) + 48;
		str[1] = (entero % 10) + 48;
	}else{
		str[0] = '0';
		str[1] = entero + 48;
	}
	 str[3] = decimal + 48;
	
}

void temInit(){
	LCDGotoXY(0,0);
	LCDstring("TEMP: ",6);
	LCDGotoXY(10,0);
	LCDstring(" C",2);
}

void initTimer(){
	/*supongo que tengo un timer de 8MHz queremos que se active cada 0.5seg
	divido por 256 me queda que cada 0.5seg hago 15625 instrucciones y pongo el 0CR0A en 125
	para que llame a la instruccion 125 veces en 0.5 segundos*/
	
	OCR0A = 125;
	TCCR0A |= (1<<WGM01);	//modo CTC
	TCCR0B |= (1<<CS02);	//prescalar de 256
	TIMSK0 |= (1<<OCIE0A); //activo interrupciones
}

ISR (TIMER0_COMPA_vect)
{
	if (counterTimer++ == 125){
		flagTimer = 1;
		counterTimer = 0;
	}	
}