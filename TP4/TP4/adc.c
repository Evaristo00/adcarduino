#include "adc.h"

volatile static uint16_t resultado;
volatile static uint8_t flagCambioValor=0;

void adc_init(){
	ADCSRA |= (1<<ADEN) | (1<<ADATE) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
	ADMUX= 0x00;
}

void adc_start(){
	ADCSRA |= (1<<ADSC);
}

uint16_t send_result(){
	return resultado;
}

uint8_t send_cambioValor(){
	return flagCambioValor;
}

void reset_cambiarValor(){
	flagCambioValor = 0;
}

ISR(ADC_vect){
	if (resultado != ADC)
		flagCambioValor = 1;
	resultado  = ADC;
}
