#include "timer0.h"

volatile static uint8_t flagTimer = 0, counterTimer = 0;


void initTimer(){
	/*supongo que tengo un timer de 8MHz queremos que se active cada 0.5seg
	divido por 256 me queda que cada 0.5seg hago 15625 instrucciones y pongo el 0CR0A en 125
	para que llame a la instruccion 125 veces en 0.5 segundos*/
	
	OCR0A = 125;
	TCCR0A |= (1<<WGM01);	//modo CTC
	TCCR0B |= (1<<CS02);	//prescalar de 256
	TIMSK0 |= (1<<OCIE0A); //activo interrupciones
}

uint8_t get_flagTimer0(){
	return flagTimer;
}

void reset_flagTimer0(){
	flagTimer = 0;
}

ISR (TIMER0_COMPA_vect)
{
	if (counterTimer++ == 125){
		flagTimer = 1;
		counterTimer = 0;
	}	
}