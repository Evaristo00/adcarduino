#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include "adc.h"
#include "timer0.h"

void separEnteroDecimal(uint16_t dato,uint8_t *entero, uint8_t *decimal);
void stringGrado(uint8_t entero, uint8_t decimal, uint8_t*str);
void temInit();


uint8_t gradoEntero,gradoDecimal;
uint8_t gradosString[4] = {'0','0','.','0'};
uint16_t grados;

int main(void)
{
	DDRB |= (1<<PINB0) | (1<<PINB1); //Configuramos los pines PB0 Y PB1 como salida del puerto B
    LCDinit();
	sei(); //Activamos interrupciones
	adc_init(); //Inicializar ADC
	adc_start();
	temInit(); //Escribe TEMP en el LCD para que luego solo se cambien los grados
	initTimer(); //Inicializa timer
	
    while (1) 
    {	
		if (get_flagTimer0()){
			separEnteroDecimal(((send_result()*5000UL)/1024),&gradoEntero,&gradoDecimal); //Guardo la parte entera y decimal de la temperatura en dos variables
			LCDGotoXY(6,0);
			stringGrado(gradoEntero,gradoDecimal,gradosString); //Generamos un string valido y lo guardamos en gradosString
			LCDstring(gradosString,4); //Mandamos string a LCD
			reset_flagTimer0();
		}
		
		if (send_cambioValor()){ //Si se realizo algun cambio en el ADC send_cambioValor retornará verdadero
			grados = ((send_result()*5000UL)/1024);
			if (grados > 240) {
				PORTB = (1<<PINB0); //Prendemos ventilación
			}else if (grados < 170){
						PORTB = (1<<PINB1); //Prendemos calefacción
					}
				else PORTB = 0; //Apagamos ambos dispositivos
			reset_cambiarValor();
		}
    }
}

void separEnteroDecimal(uint16_t dato,uint8_t *entero, uint8_t *decimal){
	//calcular el mod 10 de dato va a ser el decimal
	*decimal = dato % 10;
	*entero = dato / 10;
}

void stringGrado(uint8_t entero, uint8_t decimal, uint8_t*str){
	if(entero>9){
		str[0] = (entero / 10) + 48;
		str[1] = (entero % 10) + 48;
	}else{
		str[0] = '0';
		str[1] = entero + 48;
	}
	 str[3] = decimal + 48;
	
}

void temInit(){
	LCDGotoXY(0,0);
	LCDstring("TEMP: ",6);
	LCDGotoXY(10,0);
	LCDstring(" C",2);
}

