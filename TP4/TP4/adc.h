

#ifndef ADC_H_
#define ADC_H_
#include <avr/io.h>
#include <avr/interrupt.h>

void adc_init(void);
void adc_start(void);
uint16_t send_result(void);
uint8_t send_cambioValor(void);
void reset_cambiarValor(void);

#endif /* ADC_H_ */