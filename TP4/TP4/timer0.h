/*
 * timer0.h
 *
 * Created: 8/8/2021 9:15:28 PM
 *  Author: lenovo
 */ 


#ifndef TIMER0_H_
#define TIMER0_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void initTimer(void);
uint8_t get_flagTimer0(void);
void reset_flagTimer0(void);

#endif /* TIMER0_H_ */